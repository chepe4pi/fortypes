from django.db.transaction import atomic
from rest_auth.registration.views import RegisterView

from invite.models import Invite
from invite.serializers import InviteCodeRegisterSerializer


class InviteCodeRegisterView(RegisterView):
    serializer_class = InviteCodeRegisterSerializer

    @atomic
    def perform_create(self, serializer):
        user = super().perform_create(serializer)
        invite = Invite.objects.get(code=serializer.validated_data['invite_code'], used=False)
        invite.used = True
        invite.user_who_used = user
        invite.save()
        return user
