from django.contrib.auth.models import User
from django.db import models

from core.models import TimestampModel


class ModelHasOwner(models.Model):
    owner = models.ForeignKey(User, on_delete=models.PROTECT)

    class Meta:
        abstract = True
