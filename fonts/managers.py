from django.db import models
from django.db.models import Count, Q


class AuthorQuerySet(models.QuerySet):
    def annotate_likes_count(self):
        return self.annotate(likes_count=Count('fonts__user_relations', filter=Q(fonts__user_relations__like=True)))


class AuthorManager(models.Manager):
    def get_queryset(self):
        return AuthorQuerySet(self.model, using=self._db)


class SymbolQuerySet(models.QuerySet):
    def annotate_likes_count(self):
        return self.annotate(likes_count=Count('font__user_relations', filter=Q(font__user_relations__like=True)))


class SymbolManager(models.Manager):
    def get_queryset(self):
        return SymbolQuerySet(self.model, using=self._db)


class FontQuerySet(models.QuerySet):
    def annotate_likes_count(self):
        return self.annotate(likes_count=Count('user_relations', filter=Q(user_relations__like=True)))


class FontManager(models.Manager):
    def get_queryset(self):
        return FontQuerySet(self.model, using=self._db)
