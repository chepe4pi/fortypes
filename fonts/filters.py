import django_filters
from django_filters.rest_framework import FilterSet

from fonts.models import Font, Author, Symbol


class FontFilter(FilterSet):
    content_contains = django_filters.CharFilter(lookup_expr='contains', name='content')
    content_exact = django_filters.CharFilter(lookup_expr='exact', name='content')
    author = django_filters.NumberFilter(lookup_expr='exact', name='author_id')
    tool = django_filters.NumberFilter(lookup_expr='exact', name='tool_id')

    class Meta:
        model = Font
        fields = ['content', 'author', 'status', 'tool']


class AuthorFilter(FilterSet):
    name = django_filters.CharFilter(lookup_expr='contains')

    class Meta:
        model = Author
        fields = ['name']


class SymbolFilter(FilterSet):
    value = django_filters.CharFilter(lookup_expr='exact')
    author = django_filters.NumberFilter(lookup_expr='exact', name='font__author_id')

    class Meta:
        model = Symbol
        fields = ['value', 'author']
