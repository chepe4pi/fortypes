from django.contrib import admin

# Register your models here.
from fonts.models import Author, Font, Symbol, Tool


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    pass


@admin.register(Symbol)
class SymbolAdmin(admin.ModelAdmin):
    pass


class ToolAdmin(admin.ModelAdmin):
    model = Tool
    fields = ['type']


class SymbolInline(admin.TabularInline):
    model = Symbol
    extra = 1
    fields = ['value', 'point_one_x', 'point_one_y', 'point_two_x', 'point_two_y']


class FontAdmin(admin.ModelAdmin):
    inlines = [SymbolInline]
    model = Font
    fields = ['author', 'content', 'status', 'image', 'owner', 'tool']


admin.site.register(Font, FontAdmin)
admin.site.register(Tool, ToolAdmin)
