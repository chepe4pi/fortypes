from urllib.parse import parse_qs

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import Min, Max
from rest_framework import serializers
from rest_framework.fields import IntegerField
from rest_framework.serializers import ModelSerializer, CharField, HyperlinkedModelSerializer, PrimaryKeyRelatedField

from core.consts import IMAGE_NOT_EXIST, AUTHOR_NAME_TAKEN
from core.models import ImageObj
from core.serializers import ImageObjOutSerializer
from fonts.models import Author, Tag, Tool
from user_font_relation.models import UserFontRelation
from .models import Font, Symbol


class SymbolForFontSerializer(ModelSerializer):
    class Meta:
        model = Symbol
        fields = ('value', 'point_one_x', 'point_one_y', 'point_two_x', 'point_two_y')


class FontImageSerializer(ModelSerializer):
    class Meta:
        model = ImageObj


class FontCreateSerializer(serializers.Serializer):
    symbols = SymbolForFontSerializer(many=True, required=True)
    author_name = CharField(required=False)
    tool_id = PrimaryKeyRelatedField(source='tool', required=False, queryset=Tool.objects.all(), allow_null=True)
    owner_id = IntegerField(required=False, write_only=True)
    image_id = IntegerField(write_only=True)
    image = ImageObjOutSerializer(required=False)

    def validate_author_name(self, value):
        if Author.objects.filter(name=value).exclude(user=self.context['request'].user).exists():
            raise ValidationError(AUTHOR_NAME_TAKEN)
        return value

    def create(self, validated_data):
        user = self.context['request'].user
        symbols_data = validated_data.pop('symbols')
        author_name = validated_data.pop('author_name', None)

        try:
            image_id = ImageObj.objects.get(pk=validated_data.pop('image_id')).pk
        except ImageObj.DoesNotExist:
            raise ValidationError(IMAGE_NOT_EXIST)

        if author_name:
            author, created = Author.objects.get_or_create(name=author_name)
        else:
            author, _ = Author.objects.get_or_create(user=user, name=user.get_full_name())

        validated_data['author'] = author
        font = Font.objects.create(owner=user, image_id=image_id, **validated_data)
        for symbol_data in symbols_data:
            Symbol.objects.create(font=font, **symbol_data)
        font_with_likes = Font.objects.filter(id=font.id).annotate_likes_count().first()
        return font_with_likes

    # TODO PUT - update (moderator only)


class FontGetSerializer(ModelSerializer):
    symbols = SymbolForFontSerializer(many=True)
    author_name = serializers.CharField(source='author.name')
    image = ImageObjOutSerializer(required=False)
    tool_type = serializers.CharField(source='tool.type', required=False)
    crop_from = serializers.SerializerMethodField()
    crop_to = serializers.SerializerMethodField()
    likes_count = serializers.IntegerField()

    def _get_crop(self, instance, aggregation_func, func):
        if 'request' not in self.context.keys():
            return
        if not getattr(self.context['request'], '_request', None):
            return

        search_arg = parse_qs(self.context['request']._request.META['QUERY_STRING']).get('content_contains', None)
        if search_arg:
            values_x = instance.symbols.filter(value__in=search_arg[0]).aggregate(aggregation_func('point_one_x'),
                                                                                  aggregation_func('point_two_x'))
            values_y = instance.symbols.filter(value__in=search_arg[0]).aggregate(aggregation_func('point_one_y'),
                                                                                  aggregation_func('point_two_y'))
            x = func(values_x.values()) if all(values_x.values()) else None
            y = func(values_y.values()) if all(values_y.values()) else None
            return [x, y]

    def get_crop_from(self, instance):
        return self._get_crop(instance, Min, min)

    def get_crop_to(self, instance):
        return self._get_crop(instance, Max, max)

    class Meta:
        model = Font
        fields = ('content', 'status', 'id', 'symbols', 'owner_id', 'image_id', 'image', 'author_name', 'tool_type',
                  'crop_from', 'crop_to', 'likes_count')


class FontShortSerializer(HyperlinkedModelSerializer):
    image_original = serializers.CharField(source='image.image_original')

    class Meta:
        model = Font
        fields = ('content', 'id', 'image_original')


class CountSerializer(serializers.Serializer):
    count = serializers.IntegerField()


class NameListSerializer(serializers.Serializer):
    names = serializers.ListField()


class AuthorSerializer(ModelSerializer):
    likes_count = serializers.IntegerField()
    works_count = serializers.SerializerMethodField()
    last_works = serializers.SerializerMethodField()

    def get_works_count(self, instance):
        return Font.objects.filter(author=instance).count()

    def get_last_works(self, instance):
        return FontShortSerializer(
            Font.objects.filter(author=instance).order_by('-id')[:settings.WORKS_COUNT_IN_AUTHOR_PAGE], many=True,
            context={'request': self.context.get('request', None)}).data

    class Meta:
        model = Author
        fields = ('id', 'name', 'likes_count', 'works_count', 'last_works')


class AuthorInfoSerializer(ModelSerializer):
    likes_count = serializers.IntegerField()
    works_count = serializers.SerializerMethodField()
    symbols_list = serializers.SerializerMethodField()

    def get_works_count(self, instance):
        return Font.objects.filter(author=instance).count()

    def get_symbols_list(self, instance):
        return list(
            Symbol.objects.filter(font__author=instance).order_by('value').values_list('value', flat=True).distinct())

    class Meta:
        model = Author
        fields = ('id', 'name', 'likes_count', 'works_count', 'symbols_list')


class SymbolSerializer(ModelSerializer):
    likes_count = serializers.IntegerField()
    font_image = serializers.CharField(source='font.image.image_original')
    created_at = serializers.CharField(source='font.created_at')

    class Meta:
        model = Symbol
        fields = ('point_one_x', 'point_one_y', 'point_two_x', 'point_two_y', 'font_image', 'likes_count', 'created_at',
                  'value', 'id')


class TagSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = ('text', 'owner_id')

    def validate(self, attrs):
        attrs['owner_id'] = self.context['request'].user.id
        return attrs


class ToolSerializer(ModelSerializer):
    class Meta:
        model = Tool
        fields = ('id', 'type')
