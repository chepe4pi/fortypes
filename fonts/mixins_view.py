from rest_framework.permissions import IsAuthenticatedOrReadOnly

from fonts.models import Author


class AnnotateLikesCountMixin:
    def get_queryset(self):
        return super().get_queryset().annotate_likes_count()


class BaseAuthorViewMixin:
    queryset = Author.objects.all().order_by('-created_at')
    permission_classes = (IsAuthenticatedOrReadOnly,)
