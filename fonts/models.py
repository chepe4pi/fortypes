from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields.related import OneToOneField
from django.contrib.postgres.fields import JSONField

from accounts.models import ModelHasOwner
from core.models import ImageObj, TimestampModel
from fonts.managers import AuthorManager, SymbolManager, FontManager

STATUS_PRIVATE = 'private'
STATUS_ON_REVIEW = 'on_review'
STATUS_PUBLIC = 'public'

STATUS_CHOICES = (
    (STATUS_PRIVATE, STATUS_PRIVATE),
    (STATUS_ON_REVIEW, STATUS_ON_REVIEW),
    (STATUS_PUBLIC, STATUS_PUBLIC)
)


class Author(TimestampModel):
    name = models.CharField(max_length=500, help_text='author_name', unique=True)
    contacts = JSONField(null=True, blank=True)
    user = models.OneToOneField(User, null=True, on_delete=models.PROTECT, blank=True)

    objects = AuthorManager()

    def __str__(self):
        return 'Author {}'.format(self.name)


class Tool(models.Model):
    type = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return 'Tool {}'.format(self.type)


class Font(ModelHasOwner, TimestampModel):
    author = models.ForeignKey(Author, db_index=False, on_delete=models.PROTECT, related_name='fonts')
    content = models.CharField(max_length=200, help_text='content')
    status = models.SlugField(choices=STATUS_CHOICES, default=STATUS_ON_REVIEW)
    image = OneToOneField(ImageObj, on_delete=models.CASCADE)
    tool = models.ForeignKey(Tool, on_delete=models.PROTECT, related_name='fonts_tools', null=True, blank=True)

    objects = FontManager()

    def __str__(self):
        return '{} Id {} by {}'.format(self.content, self.pk, self.author.name)


class Symbol(models.Model):
    value = models.CharField(max_length=1)
    font = models.ForeignKey(Font, related_name='symbols', on_delete=models.CASCADE)
    point_one_x = models.FloatField()
    point_one_y = models.FloatField()
    point_two_x = models.FloatField()
    point_two_y = models.FloatField()

    objects = SymbolManager()

    def __str__(self):
        return '{} of {}, Id {}'.format(self.value, self.font.content, self.id)


class Tag(ModelHasOwner, models.Model):
    text = models.CharField(max_length=50, unique=True)
