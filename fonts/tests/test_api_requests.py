import json
from unittest import skip

from django.test import override_settings, RequestFactory
from django.urls import reverse
from rest_framework import status
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.test import APITestCase

from accounts.tests.factories import UserFactory
from core.models import ImageObj
from core.serializers import ImageObjOutSerializer
from core.tests import AuthorizeForTestsMixin
from fonts.models import Font, Symbol, STATUS_ON_REVIEW, Author, Tag, STATUS_PRIVATE, Tool
from fonts.serializers import SymbolForFontSerializer, CountSerializer, FontGetSerializer, AuthorSerializer, \
    TagSerializer, FontShortSerializer, AuthorInfoSerializer, SymbolSerializer, ToolSerializer
from fonts.tests.factories import ImageObjFactory, FontFactory, SymbolFactory, AuthorFactory, TagFactory, \
    ToolFactory
from user_font_relation.tests.factories import AdminFontRelationFactory, UserFontRelationFactory


class FontCreateTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(FontCreateTestCase, self).setUp()
        self.url = reverse("font-list")
        self.image_id = ImageObjFactory().pk
        self.tool_id = ToolFactory().pk
        self.data = {
            "title": "title",
            "content": "ab",
            "image_id": self.image_id,
            "symbols": [
                {"value": "a",
                 "point_one_x": 0.2,
                 "point_one_y": 0.3,
                 "point_two_x": 0.4,
                 "point_two_y": 0.5,
                 },
                {"value": "b",
                 "point_one_x": 0.6,
                 "point_one_y": 0.7,
                 "point_two_x": 0.8,
                 "point_two_y": 0.9,
                 },
            ],
            "tool_id": self.tool_id
        }

    def test_create_font_with_image_no_author_name(self):
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        font = Font.objects.filter(owner=self.user).annotate_likes_count().first()
        response.data['image']['image_original'] = '/media/' + response.data['image']['image_original'].split('/')[-1]
        response.data['image']['image_thumbnail'] = '/media/' + response.data['image']['image_thumbnail'].split('/')[-1]
        serialized_font = FontGetSerializer(font).data
        self.assertEqual(response.data, serialized_font)
        self.assertEqual(serialized_font['author_name'], 'Alex Tester')
        author = Author.objects.get()
        self.assertEqual(author.user, self.user)
        self.assertEqual(author.name, self.user.get_full_name())

        symbol_1_data = SymbolForFontSerializer(Symbol.objects.all()[0]).data
        symbol_2_data = SymbolForFontSerializer(Symbol.objects.all()[1]).data
        self.assertIn(symbol_1_data, response.data['symbols'])
        self.assertIn(symbol_2_data, response.data['symbols'])

    def test_create_font_with_image_new_author_name(self):
        self.data['author_name'] = 'Some Author'
        self.assertEqual(Author.objects.all().count(), 0)
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        font = Font.objects.filter(owner=self.user).annotate_likes_count().first()
        response.data['image']['image_original'] = '/media/' + response.data['image']['image_original'].split('/')[-1]
        response.data['image']['image_thumbnail'] = '/media/' + response.data['image']['image_thumbnail'].split('/')[-1]
        serialized_font = FontGetSerializer(font).data
        self.assertEqual(response.data, serialized_font)
        self.assertEqual(font.author.name, 'Some Author')
        author = Author.objects.get()
        self.assertEqual(author.user, None)
        self.assertEqual(font.author, author)

        symbol_1_data = SymbolForFontSerializer(Symbol.objects.all()[0]).data
        symbol_2_data = SymbolForFontSerializer(Symbol.objects.all()[1]).data
        self.assertIn(symbol_1_data, response.data['symbols'])
        self.assertIn(symbol_2_data, response.data['symbols'])

    def test_create_font_with_image_exist_author(self):
        self.author = AuthorFactory(name='Some Author', user=self.user)
        self.assertEqual(Author.objects.all().count(), 1)
        self.data['author_name'] = 'Some Author'
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        font = Font.objects.filter(owner=self.user).annotate_likes_count().first()
        response.data['image']['image_original'] = '/media/' + response.data['image']['image_original'].split('/')[-1]
        response.data['image']['image_thumbnail'] = '/media/' + response.data['image']['image_thumbnail'].split('/')[-1]
        serialized_font = FontGetSerializer(font).data
        self.assertEqual(response.data, serialized_font)

        self.assertEqual(Author.objects.all().count(), 1)
        self.assertEqual(font.author.name, 'Some Author')
        self.assertEqual(font.author, self.author)

        symbol_1_data = SymbolForFontSerializer(Symbol.objects.all()[0]).data
        symbol_2_data = SymbolForFontSerializer(Symbol.objects.all()[1]).data
        self.assertIn(symbol_1_data, response.data['symbols'])
        self.assertIn(symbol_2_data, response.data['symbols'])

    def test_create_font_with_image_exist_author_other_user(self):
        other_user = UserFactory(username='other_admin')
        self.author = AuthorFactory(name='Some Author', user=other_user)
        self.assertEqual(Author.objects.all().count(), 1)
        self.data['author_name'] = 'Some Author'
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Font.objects.all().count(), 0)

    def test_create_font_with_tool(self):
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        font = Font.objects.filter(owner=self.user).annotate_likes_count().first()
        response.data['image']['image_original'] = '/media/' + response.data['image']['image_original'].split('/')[-1]
        response.data['image']['image_thumbnail'] = '/media/' + response.data['image']['image_thumbnail'].split('/')[-1]
        serialized_font = FontGetSerializer(font).data
        self.assertEqual(response.data, serialized_font)
        self.assertEqual(Font.objects.all().count(), 1)
        self.assertEqual(Tool.objects.all().count(), 1)
        self.assertEqual(font.tool.pk, self.tool_id)

    def test_create_font_without_tool(self):
        del self.data["tool_id"]
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        font = Font.objects.filter(owner=self.user).annotate_likes_count().first()
        response.data['image']['image_original'] = '/media/' + response.data['image']['image_original'].split('/')[-1]
        response.data['image']['image_thumbnail'] = '/media/' + response.data['image']['image_thumbnail'].split('/')[-1]
        serialized_font = FontGetSerializer(font).data
        self.assertEqual(response.data, serialized_font)
        self.assertEqual(Font.objects.all().count(), 1)

    def test_create_font_with_wrong_tool_id(self):
        self.data["tool_id"] = 1
        response = self.client.post(self.url, data=json.dumps(self.data), content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Font.objects.all().count(), 0)
        self.assertEqual(response.data["tool_id"][0], PrimaryKeyRelatedField.default_error_messages["does_not_exist"].format(pk_value=1))


class FontsGetTestCase(APITestCase):
    def setUp(self):
        super(FontsGetTestCase, self).setUp()
        self.user = UserFactory(is_superuser=True, username='user_admin_1')
        self.user_2 = UserFactory(username='user_guy_1')
        self.client.force_authenticate(user=self.user)
        self.author = AuthorFactory(name='Mr. Writer')
        self.author_2 = AuthorFactory(name='Mr. Writer 2')
        self.tool = ToolFactory(type='Tool')
        self.tool_2 = ToolFactory(type='Tool 2')
        self.font_1 = FontFactory(owner=self.user, content='THE', author=self.author_2, tool=self.tool_2)
        self.font_2 = FontFactory(owner=self.user, author=self.author, content='HER')
        self.admin_font_relation = AdminFontRelationFactory(user=self.user, font=self.font_1)
        self.admin_font_relation_2 = AdminFontRelationFactory(user=self.user, font=self.font_2)
        self.user_font_relation = UserFontRelationFactory(user=self.user, font=self.font_1, like=True)
        self.user_font_relation_2 = UserFontRelationFactory(user=self.user_2, font=self.font_1, like=True)
        self.symbol_1 = SymbolFactory(font=self.font_2, value='E',
                                      point_one_x=100,
                                      point_one_y=70,
                                      point_two_x=120,
                                      point_two_y=121)
        self.symbol_2 = SymbolFactory(font=self.font_2, value='R',
                                      point_one_x=110,
                                      point_one_y=60,
                                      point_two_x=220,
                                      point_two_y=221
                                      )
        self.symbol_3 = SymbolFactory(font=self.font_1)
        self.url = reverse("font-list")

    def test_get_font_with_image(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response.data['results'][0]['image']['image_original'] = '/media/' + \
                                                      response.data['results'][0]['image']['image_original'].split('/')[-1]
        response.data['results'][1]['image']['image_original'] = '/media/' + \
                                                      response.data['results'][1]['image']['image_original'].split('/')[-1]
        response.data['results'][0]['image']['image_thumbnail'] = '/media/' + \
                                                       response.data['results'][0]['image']['image_thumbnail'].split('/')[-1]
        response.data['results'][1]['image']['image_thumbnail'] = '/media/' + \
                                                       response.data['results'][1]['image']['image_thumbnail'].split('/')[-1]
        font_1 = Font.objects.filter(id=self.font_1.pk).annotate_likes_count().first()
        font_2 = Font.objects.filter(id=self.font_2.pk).annotate_likes_count().first()
        self.assertEqual(response.data['results'], [FontGetSerializer(font_2).data, FontGetSerializer(font_1).data])
        self.assertEqual(response.data['results'][1]['author_name'], 'Mr. Writer 2')
        self.assertEqual(response.data['results'][0]['author_name'], 'Mr. Writer')

        symbol_1_data = SymbolForFontSerializer(self.symbol_1).data
        symbol_2_data = SymbolForFontSerializer(self.symbol_2).data
        symbol_3_data = SymbolForFontSerializer(self.symbol_3).data
        self.assertIn(symbol_1_data, response.data['results'][0]['symbols'])
        self.assertIn(symbol_2_data, response.data['results'][0]['symbols'])
        self.assertIn(symbol_3_data, response.data['results'][1]['symbols'])

    def test_get_font_search_no_filter(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_get_font_search_no_filter_one_no_moderated(self):
        self.user_not_admin = UserFactory(is_superuser=False, username='user_no_admin')
        self.client.force_authenticate(user=self.user_not_admin)
        self.font_1.status = STATUS_ON_REVIEW
        self.font_1.save()
        self.admin_font_relation.moderated = False
        self.admin_font_relation.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_get_font_search_no_filter_one_no_moderated_private(self):
        self.user_not_admin = UserFactory(is_superuser=False, username='user_no_admin')
        self.client.force_authenticate(user=self.user_not_admin)
        self.font_1.status = STATUS_PRIVATE
        self.font_1.save()
        self.admin_font_relation.moderated = False
        self.admin_font_relation.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_font_search_no_filter_one_no_moderated_if_admin(self):
        self.client.force_authenticate(user=UserFactory(is_superuser=True, username='user_admin'))
        self.admin_font_relation.moderated = False
        self.admin_font_relation.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_get_font_filter_author_id(self):
        context = {'request': RequestFactory().get(self.url)}
        response = self.client.get(self.url, data={'author': self.author_2.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        fonts = Font.objects.filter(id=self.font_1.pk).annotate_likes_count()
        self.assertEqual(response.data['results'], [FontGetSerializer(fonts[0], context=context).data])

    def test_get_font_filter_tool_id(self):
        context = {'request': RequestFactory().get(self.url)}
        response = self.client.get(self.url, data={'tool': self.tool_2.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        fonts = Font.objects.filter(id=self.font_1.pk).annotate_likes_count()
        self.assertEqual(response.data['results'], [FontGetSerializer(fonts[0], context=context).data])

    def test_get_font_search_one(self):
        response = self.client.get(self.url, data={'content_contains': 'ER'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_font_search_two(self):
        response = self.client.get(self.url, data={'content_contains': 'HE'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, response.data['count'])
        self.assertEqual(len(response.data['results']), 2)

    def test_get_font_search_three(self):
        response = self.client.get(self.url, data={'content_contains': 'T'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_font_search_empty_content_exact(self):
        response = self.client.get(self.url, data={'content_exact': 'HE'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], [])

    def test_get_font_search_ok_content_exact(self):
        response = self.client.get(self.url, data={'content_exact': 'HER'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_font_status_in_review(self):
        self.font_1.status = STATUS_ON_REVIEW
        self.font_1.save()
        response = self.client.get(self.url, data={'status': STATUS_ON_REVIEW})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['id'], self.font_1.id)

    def test_get_font_likes_count(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        fonts = Font.objects.all().annotate_likes_count()
        self.assertEqual(response.data['results'][1]['likes_count'], fonts[0].likes_count)
        self.assertEqual(response.data['results'][0]['likes_count'], fonts[1].likes_count)
        self.assertEqual(2, fonts[0].likes_count)
        self.assertEqual(0, fonts[1].likes_count)

    def test_crop_from_to(self):
        response = self.client.get(self.url, data={'content_contains': 'ER', 'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'][0]['crop_from'], [100.0, 60.0])
        self.assertEqual(response.data['results'][0]['crop_to'], [220.0, 221.0])
        self.assertEqual(len(response.data['results']), 1)


class FontDeleteTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(FontDeleteTestCase, self).setUp()
        self.font_1 = FontFactory(owner=self.user, content='THE')
        self.admin_font_relation = AdminFontRelationFactory(user=self.user, font=self.font_1)
        self.symbol_1 = SymbolFactory(font=self.font_1)
        self.url = reverse("font-detail", args=(self.font_1.pk,))

    def test_font_delete(self):
        self.assertEqual(Font.objects.all().count(), 1)
        self.assertEqual(Symbol.objects.all().count(), 1)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Font.objects.all().count(), 0)
        self.assertEqual(Symbol.objects.all().count(), 0)

    def test_font_delete_not_owner(self):
        self.client.force_authenticate(user=UserFactory(username='user_2'))
        self.assertEqual(Font.objects.all().count(), 1)
        self.assertEqual(Symbol.objects.all().count(), 1)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Font.objects.all().count(), 1)
        self.assertEqual(Symbol.objects.all().count(), 1)


class UploadImageTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(UploadImageTestCase, self).setUp()
        self.url = reverse("font-upload", args=('test_file.jpg',))
        self.test_file = open('fonts/tests/test_file.jpg', 'rb').read()

    def test_upload_image(self):
        response = self.client.post(self.url, {'image': self.test_file}, format='multipart')
        image_obj = ImageObj.objects.get()
        self.assertEqual(response.data, ImageObjOutSerializer(image_obj).data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ImageObj.objects.all()[0].image_original.delete()


@skip('do we need it?')
class FontsCountTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(FontsCountTestCase, self).setUp()
        self.author = AuthorFactory()
        self.tool = ToolFactory()
        self.font_1 = FontFactory(owner=self.user, content='THE', author=self.author, tool=self.tool)
        self.font_2 = FontFactory(owner=self.user, content='HER', author=self.author, tool=self.tool)
        self.symbol_1 = SymbolFactory(font=self.font_1)
        self.symbol_2 = SymbolFactory(font=self.font_1)
        self.symbol_3 = SymbolFactory(font=self.font_2)
        self.url = reverse("fonts-count-list")

    def test_get_fonts_count(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], CountSerializer(data={'count': 2}).initial_data)

    def test_get_fonts_count_filter(self):
        response = self.client.get(self.url, data={'content_exact': 'HER'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], CountSerializer(data={'count': 1}).initial_data)


class AuthorsNamesTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(AuthorsNamesTestCase, self).setUp()

        self.author = AuthorFactory(name='Mr. Writer')
        self.author_2 = AuthorFactory(name='Mr. Peter', user=self.user)
        self.author_3 = AuthorFactory(name='Mr. Killer')

        self.url = reverse("names_list-list")

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], {'names': ['Mr. Writer', 'Mr. Peter', 'Mr. Killer']})

    def test_get_filter(self):
        response = self.client.get(self.url, data={'name': 'ter'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], {'names': ['Mr. Writer', 'Mr. Peter']})


@skip("probably don't need")
class SymbolsCountTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(SymbolsCountTestCase, self).setUp()
        self.author = AuthorFactory()
        self.tool = ToolFactory()
        self.font_1 = FontFactory(owner=self.user, content='THE', author=self.author, tool=self.tool)
        self.font_2 = FontFactory(owner=self.user, content='HER', author=self.author, tool=self.tool)
        self.symbol_1 = SymbolFactory(font=self.font_1)
        self.symbol_2 = SymbolFactory(font=self.font_1)
        self.symbol_3 = SymbolFactory(font=self.font_2)
        self.url = reverse("symbols-count-list")

    def test_get_symbols_count(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], CountSerializer(data={'count': 3}).initial_data)


class AuthorsTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(AuthorsTestCase, self).setUp()
        self.user_2 = UserFactory(username='any name')
        self.tool = ToolFactory()
        self.author = AuthorFactory(name='Mr. Writer', user=self.user_2)
        self.font_1 = FontFactory(author=self.author, owner=self.user, tool=self.tool)
        SymbolFactory(font=self.font_1, value='C')
        SymbolFactory(font=self.font_1, value='A')
        SymbolFactory(font=self.font_1, value='B')
        SymbolFactory(font=self.font_1, value='1')
        SymbolFactory(font=self.font_1, value='C')
        SymbolFactory(font=self.font_1, value='B')
        self.admin_font_relation = UserFontRelationFactory(user=self.user, font=self.font_1, like=True)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_2, font=self.font_1, like=True)
        self.font_2 = FontFactory(author=self.author, owner=self.user, tool=self.tool)
        self.admin_font_relation = UserFontRelationFactory(user=self.user, font=self.font_2, like=True)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_2, font=self.font_2, like=True)
        self.font_3 = FontFactory(author=self.author, owner=self.user, tool=self.tool)

        self.author_2 = AuthorFactory(name='Mr. Peter', user=self.user)
        self.font_4 = FontFactory(author=self.author_2, owner=self.user, tool=self.tool)
        self.admin_font_relation = UserFontRelationFactory(user=self.user, font=self.font_4, like=True)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_2, font=self.font_4, like=True)

        self.user_3 = UserFactory(username='any name 2')
        self.user_4 = UserFactory(username='any name 3')
        self.author_3 = AuthorFactory(name='Mr. Killer', user=self.user_3)
        self.font_5 = FontFactory(author=self.author_3, owner=self.user, tool=self.tool)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_2, font=self.font_5, like=True)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_3, font=self.font_5, like=False)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_4, font=self.font_5, like=False)
        self.font_6 = FontFactory(author=self.author_3, owner=self.user, tool=self.tool)

        self.url = reverse("author-list")
        self.url_detail = reverse("author-detail", args=(self.author.pk,))

    def test_get_one_author(self):
        authors = Author.objects.filter(id=self.author.pk).annotate_likes_count()
        response = self.client.get(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['symbols_list'], ['1', 'A', 'B', 'C'])
        self.assertEqual(AuthorInfoSerializer(authors[0]).data, response.data)

    @override_settings(WORKS_COUNT_IN_AUTHOR_PAGE=2)
    def test_get_all(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        context = {'request': RequestFactory().get(self.url)}

        self.assertEqual(len(response.data['results']), 3)
        self.assertEqual(response.data['results'][1]['name'], 'Mr. Peter')
        self.assertEqual(response.data['results'][1]['likes_count'], 2)
        self.assertEqual(response.data['results'][1]['works_count'], 1)

        self.assertEqual(response.data['results'][0]['name'], 'Mr. Killer')
        self.assertEqual(response.data['results'][0]['likes_count'], 1)
        self.assertEqual(response.data['results'][0]['works_count'], 2)

        self.assertEqual(response.data['results'][2]['name'], 'Mr. Writer')
        self.assertEqual(response.data['results'][2]['likes_count'], 4)
        self.assertEqual(response.data['results'][2]['works_count'], 3)
        self.assertEqual(response.data['results'][2]['last_works'],
                         FontShortSerializer([self.font_3, self.font_2], many=True, context=context).data)

        authors = Author.objects.filter(
            id__in=[self.author.pk, self.author_3.pk, self.author_2.pk]).annotate_likes_count()
        self.assertEqual(3, len(response.data['results']))
        self.assertIn(AuthorSerializer(authors[0]).data, response.data['results'])
        self.assertIn(AuthorSerializer(authors[1]).data, response.data['results'])
        self.assertIn(AuthorSerializer(authors[2]).data, response.data['results'])

    def test_get_filter(self):
        response = self.client.get(self.url, data={'name': 'ter'})
        context = {'request': RequestFactory().get(self.url)}
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        authors = Author.objects.filter(id__in=[self.author.pk, self.author_2.pk]).annotate_likes_count()

        self.assertEqual(len(response.data['results']), 2)
        self.assertEqual(response.data['results'][0]['name'], 'Mr. Peter')
        self.assertEqual(response.data['results'][0]['likes_count'], 2)
        self.assertEqual(response.data['results'][0]['works_count'], 1)

        self.assertEqual(response.data['results'][1]['name'], 'Mr. Writer')
        self.assertEqual(response.data['results'][1]['likes_count'], 4)
        self.assertEqual(response.data['results'][1]['works_count'], 3)

        self.assertEqual(2, len(response.data['results']))
        self.assertIn(AuthorSerializer(authors[0]).data, response.data['results'])
        self.assertIn(AuthorSerializer(authors[1]).data, response.data['results'])

    def test_get_order_created(self):
        response = self.client.get(self.url, data={'ordering': 'created_at'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][0]['name'], self.author.name)
        self.assertEqual(response.data['results'][1]['name'], self.author_2.name)
        self.assertEqual(response.data['results'][2]['name'], self.author_3.name)

    def test_get_order_minus_created(self):
        response = self.client.get(self.url, data={'ordering': '-created_at'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][2]['name'], self.author.name)
        self.assertEqual(response.data['results'][1]['name'], self.author_2.name)
        self.assertEqual(response.data['results'][0]['name'], self.author_3.name)

    def test_get_order_likes_count(self):
        response = self.client.get(self.url, data={'ordering': 'likes_count'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][0]['name'], self.author_3.name)
        self.assertEqual(response.data['results'][0]['likes_count'], 1)
        self.assertEqual(response.data['results'][1]['name'], self.author_2.name)
        self.assertEqual(response.data['results'][1]['likes_count'], 2)
        self.assertEqual(response.data['results'][2]['name'], self.author.name)
        self.assertEqual(response.data['results'][2]['likes_count'], 4)

    def test_get_order_minus_likes_count(self):
        response = self.client.get(self.url, data={'ordering': '-likes_count'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][0]['name'], self.author.name)
        self.assertEqual(response.data['results'][0]['likes_count'], 4)
        self.assertEqual(response.data['results'][1]['name'], self.author_2.name)
        self.assertEqual(response.data['results'][1]['likes_count'], 2)
        self.assertEqual(response.data['results'][2]['name'], self.author_3.name)
        self.assertEqual(response.data['results'][2]['likes_count'], 1)


class TagsTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(TagsTestCase, self).setUp()
        self.tag_1 = TagFactory(text='tag text 1', owner=self.user)
        self.tag_2 = TagFactory(text='tag text 2', owner=self.user)
        self.tag_3 = TagFactory(text='tag text 3', owner=self.user)
        self.url = reverse("tag-list")

    def test_get_all(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], TagSerializer([self.tag_1, self.tag_2, self.tag_3], many=True).data)

    def test_create_tag(self):
        self.assertEqual(Tag.objects.all().count(), 3)
        response = self.client.post(self.url, data={'text': 'tag text new'})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.json())
        self.assertEqual(Tag.objects.all().count(), 4)
        self.assertEqual(Tag.objects.all().last().owner, self.user)


class SymbolsTestCase(AuthorizeForTestsMixin, APITestCase):
    def setUp(self):
        super(SymbolsTestCase, self).setUp()
        self.user_2 = UserFactory(username='any name')
        self.author = AuthorFactory(name='Mr. Writer', user=self.user_2)
        self.tool = ToolFactory(type='Tool')
        self.font_1 = FontFactory(author=self.author, owner=self.user, tool=self.tool)
        self.symbol_1 = SymbolFactory(font=self.font_1, value='C')
        self.symbol_2 = SymbolFactory(font=self.font_1, value='A')
        self.symbol_3 = SymbolFactory(font=self.font_1, value='B')
        self.symbol_4 = SymbolFactory(font=self.font_1, value='1')
        self.symbol_5 = SymbolFactory(font=self.font_1, value='C')
        self.symbol_6 = SymbolFactory(font=self.font_1, value='B')

        self.user_3 = UserFactory(username='any name 2')
        self.author_2 = AuthorFactory(name='Mr. Writer 2', user=self.user_3)
        self.tool_2 = ToolFactory(type='Tool 2')
        self.font_2 = FontFactory(author=self.author_2, owner=self.user_2, tool=self.tool_2)
        self.symbol_7 = SymbolFactory(font=self.font_2, value='C')
        self.symbol_8 = SymbolFactory(font=self.font_2, value='C')
        self.symbol_9 = SymbolFactory(font=self.font_2, value='B')

        self.admin_font_relation = UserFontRelationFactory(user=self.user, font=self.font_1, like=True)
        self.admin_font_relation = UserFontRelationFactory(user=self.user_2, font=self.font_1, like=True)
        self.admin_font_relation = UserFontRelationFactory(user=self.user, font=self.font_2, like=True)

        self.url = reverse("symbols-list")

    def test_get_all(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 9)

    def test_get_filter(self):
        response = self.client.get(self.url, data={'author': self.author_2.pk, 'value': 'C'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)
        symbols = Symbol.objects.filter(id__in=[self.symbol_8.pk, self.symbol_7.pk]).annotate_likes_count()
        for font_data in SymbolSerializer(symbols, many=True).data:
            self.assertIn(font_data, response.data['results'])

    def test_get_order_created(self):
        response = self.client.get(self.url, data={'ordering': 'font__created_at'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertIn(response.data['results'][0]['id'], self.font_1.symbols.all().values_list('id', flat=True))
        self.assertIn(response.data['results'][-1]['id'], self.font_2.symbols.all().values_list('id', flat=True))

    def test_get_order_minus_created(self):
        response = self.client.get(self.url, data={'ordering': '-font__created_at'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertIn(response.data['results'][0]['id'], self.font_2.symbols.all().values_list('id', flat=True))
        self.assertIn(response.data['results'][-1]['id'], self.font_1.symbols.all().values_list('id', flat=True))

    def test_get_order_likes_count(self):
        response = self.client.get(self.url, data={'ordering': 'likes_count'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][0]['likes_count'], 1)
        self.assertEqual(response.data['results'][-1]['likes_count'], 2)

    def test_get_order_minus_likes_count(self):
        response = self.client.get(self.url, data={'ordering': '-likes_count'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(response.data['results'][0]['likes_count'], 2)
        self.assertEqual(response.data['results'][-1]['likes_count'], 1)


class ToolsTestCase(AuthorizeForTestsMixin, APITestCase):

    def setUp(self):
        super(ToolsTestCase, self).setUp()
        self.tool_1 = ToolFactory(type='tool 1')
        self.tool_2 = ToolFactory(type='tool 2')
        self.tool_3 = ToolFactory(type='tool 3')
        self.url = reverse("tools-list")

    def test_get_all(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], ToolSerializer([self.tool_1, self.tool_2, self.tool_3], many=True).data)
