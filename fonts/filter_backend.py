from rest_framework.filters import BaseFilterBackend

from fonts.models import STATUS_PUBLIC, STATUS_PRIVATE


class IsAdminOrModeratedFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.is_authenticated and (request.user.is_superuser or request.user.is_staff):
            return queryset
        return queryset.exclude(status=STATUS_PRIVATE)
