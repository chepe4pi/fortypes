from django.contrib import admin

from user_font_relation.models import UserFontRelation, AdminFontRelation


@admin.register(UserFontRelation)
class UserFontRelationAdmin(admin.ModelAdmin):
    pass


@admin.register(AdminFontRelation)
class AdminFontRelationAdmin(admin.ModelAdmin):
    pass
