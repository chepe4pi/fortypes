"""fortypes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from accounts.views import InviteCodeRegisterView
from collections_posts.views import CollectionPostViewSet
from fonts.views import FileUploadView, FontViewSet, AuthorView, TagView, \
    AuthorInfoView, SymbolsViewSet, AuthorNameListView, ToolsViewSet
from user_font_relation.views import UserFontRelationsViewSet

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^docs/', schema_view),

    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),

    url(r'^api/invite-registration/', InviteCodeRegisterView.as_view(), name='invite-registration'),

    url(r'^api/font_upload/(?P<filename>[^/]+)$', FileUploadView.as_view(), name='font-upload'),

    url(r'^api/authors/$', AuthorView.as_view({'get': 'list'}), name='author-list'),
    url(r'^api/authors/(?P<pk>[^/]+)/$', AuthorInfoView.as_view({'get': 'retrieve'}), name='author-detail'),
]

router = DefaultRouter()
router.register(r'api/fonts', FontViewSet, base_name='font')
router.register(r'api/symbols', SymbolsViewSet, base_name='symbols')
router.register(r'api/tags', TagView, base_name='tag')
router.register(r'api/authors', AuthorView, base_name='author')
# router.register(r'api/fonts-count', FontCountView, base_name='fonts-count')
router.register(r'api/author_names', AuthorNameListView, base_name='names_list')
# router.register(r'api/symbols-count', SymbolsCountView, base_name='symbols-count')
router.register(r'api/fonts-relations', UserFontRelationsViewSet, base_name='fonts-relation')
router.register(r'api/collections', CollectionPostViewSet, base_name='collection')
router.register(r'api/tools', ToolsViewSet, base_name='tools')

urlpatterns += router.urls
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
