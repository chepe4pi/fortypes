from django.contrib import admin

from core.models import ImageObj


@admin.register(ImageObj)
class ImageObjAdmin(admin.ModelAdmin):
    pass
