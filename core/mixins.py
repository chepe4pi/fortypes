from rest_framework.response import Response

from fonts.serializers import CountSerializer, NameListSerializer

#
# class CountViewMixin:
#     serializer_class = CountSerializer
#
#     def list(self, request, *args, **kwargs):
#         queryset = self.filter_queryset(self.get_queryset())
#         page = self.paginate_queryset(queryset)
#         if page is not None:
#             serializer = self.get_serializer(page, many=True)
#             return self.get_paginated_response(serializer.data)
#         serializer = self.get_serializer(data={'count': queryset.count()})
#         serializer.is_valid()
#         return Response(serializer.data)
#


class NameListViewMixin:
    serializer_class = NameListSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(data={'names': queryset.values_list('name', flat=True)})
            serializer.is_valid(raise_exception=True)
            return self.get_paginated_response(serializer.data)
